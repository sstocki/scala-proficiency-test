package silverstar.proficiency.task

import scala.util.{Failure, Success, Try}

object SolutionApp extends App {

  private val MAX_LOC: Long           = 1000000000000000L
  private val locationMessage: String = s"Location has to be integer lower than $MAX_LOC"

  println("\n\nProgram started.")
  if (args.length == 1) {
    val arg: String = args(0)
    Try(arg.toLong) match {
      case Success(location) if location < MAX_LOC =>
        val solution = Solution.calculateDistance(location)
        println(s"Distance to the center for location $location is $solution.")
      case Success(other) => println(s"Given location $other is too big, maximum location is $MAX_LOC.")
      case Failure(_)     => println(locationMessage)
    }
  } else {
    println("Location not give. " + locationMessage)
  }
}
