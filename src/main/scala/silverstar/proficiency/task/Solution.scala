package silverstar.proficiency.task

import scala.annotation.tailrec
import scala.math.BigDecimal.RoundingMode
import scala.math.{abs, sqrt}

object Solution {

  private def roundToNextOdd(d: Double): Long = {
    val longRoundedUp = BigDecimal(d).setScale(0, RoundingMode.UP).toLongExact
    if (longRoundedUp % 2 == 0) longRoundedUp + 1 else longRoundedUp
  }

  private[task] def minN(location: Long): Long = roundToNextOdd(sqrt(location))

  private[task] def manhattanDistance(rowA: Long, colA: Long, rowB: Long, colB: Long): Long =
    abs(rowA - rowB) + abs(colA - colB)

  private[task] def center(oddN: Long): Long = (oddN - 1) / 2L

  def calculateDistance(location: Long): Long = {
    val n: Long = minN(location)

    val (rowA: Long, colA: Long) = {
      if (n == 1) {
        (0L, 0L)
      } else {
        val MaxIndex: Long                    = n - 1L
        val MinIndex: Long                    = 0L
        val bottomRightCornerValue: Long      = n * n
        val topLeftCornerValue: Long          = (MaxIndex * MaxIndex) + 1
        val bottomRightCornerOneUpValue: Long = (n - 2) * (n - 2) + 1

        @tailrec
        def iterClockwise(temp: Long, row: Long, col: Long): (Long, Long) = {
          if (temp == location) {
            (row, col)
          } else {
            val (nextRow, nextCol) = (row, col) match {
              case (MaxIndex, c) => if (c > MinIndex) (row, col - 1) else (row - 1, col)
              case (r, MinIndex) => if (r > MinIndex) (row - 1, col) else (row, col + 1)
              case (MinIndex, c) => if (c < MaxIndex) (row, col + 1) else (row + 1, col)
              case (r, MaxIndex) => if (r < MaxIndex) (row + 1, col) else (-1L, -1L)
              case (_, _)        => throw new UnsupportedOperationException()

            }
            iterClockwise(temp - 1, nextRow, nextCol)
          }
        }

        @tailrec
        def iterAntiClockwise(temp: Long, row: Long, col: Long): (Long, Long) = {
          if (temp == location) {
            (row, col)
          } else {
            val (nextRow, nextCol) = (row, col) match {
              case (r, MaxIndex) => if (r > MinIndex) (row - 1, col) else (row, col - 1)
              case (MinIndex, c) => if (c > MinIndex) (row, col - 1) else (row + 1, col)
              case (r, MinIndex) => if (r < MaxIndex) (row + 1, col) else (row, col + 1)
              case (MaxIndex, c) => if (c < MaxIndex) (row, col + 1) else (-1L, -1L)
              case (_, _)        => throw new UnsupportedOperationException()

            }
            iterAntiClockwise(temp + 1, nextRow, nextCol)
          }
        }

        val (start, row, col, iter) = {
          if (location > topLeftCornerValue) {
            val isCloserToBottomRightCorner = (bottomRightCornerValue - location) < (location - topLeftCornerValue)
            if (isCloserToBottomRightCorner)
              (bottomRightCornerValue,
               MaxIndex,
               MaxIndex,
               (temp: Long, row: Long, col: Long) => iterClockwise(temp, row, col))
            else
              (topLeftCornerValue,
               MinIndex,
               MinIndex,
               (temp: Long, row: Long, col: Long) => iterAntiClockwise(temp, row, col))
          } else {
            val isCloserToTopLeftCorner = (topLeftCornerValue - location) < (location - bottomRightCornerOneUpValue)
            if (isCloserToTopLeftCorner)
              (topLeftCornerValue,
               MinIndex,
               MinIndex,
               (temp: Long, row: Long, col: Long) => iterClockwise(temp, row, col))
            else
              (bottomRightCornerOneUpValue,
               MaxIndex - 1,
               MaxIndex,
               (temp: Long, row: Long, col: Long) => iterAntiClockwise(temp, row, col))
          }
        }
        iter(start, row, col)
      }
    }

    val c: Long = center(n)
    manhattanDistance(rowA, colA, c, c)
  }
}
