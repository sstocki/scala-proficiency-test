package silverstar.proficiency.task

import org.scalameter.KeyValue
import org.scalameter.api._

object Benchmark extends Bench.OfflineReport {

  private val ranges: Gen[Int]  = Gen.range("size")(100000, 1000000, 20000)
  private val inputs: Gen[Long] = for (r <- ranges) yield r.toLong * 1000000

  performance of "Solution" in {
    measure method "calculateDistance" in {
      using(inputs) config (
        KeyValue(exec.benchRuns, 10),
        KeyValue(exec.maxWarmupRuns, 5),
        KeyValue(verbose, false)
      ) in { number =>
        Solution.calculateDistance(number)
      }
    }
  }
}
