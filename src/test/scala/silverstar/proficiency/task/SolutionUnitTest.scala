package silverstar.proficiency.task

import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class SolutionUnitTest extends AnyWordSpec with Matchers {

  "calculateDistance" when {
    val testData = Seq(
      TestData(1L, 0L),
      TestData(10L, 3L),
      TestData(12L, 3L),
      TestData(17L, 4L),
      TestData(20L, 3L),
      TestData(23L, 2L),
      TestData(1024L, 31L)
    )

    testData.foreach(t => {
      s"location is ${t.location}" should {
        s"return ${t.expectedOutput}" in {
          val actual = Solution.calculateDistance(t.location)
          actual shouldBe t.expectedOutput
        }
      }
    })
  }

  "minN" when {
    val testData = Seq(
      TestData(1L, 1L),
      TestData(12L, 5L),
      TestData(23L, 5L),
      TestData(49L, 7L),
      TestData(1024L, 33L)
    )

    testData.foreach(t => {
      s"location is ${t.location}" should {
        s"return ${t.expectedOutput}" in {
          val actual = Solution.minN(t.location)
          actual shouldBe t.expectedOutput
        }
      }
    })
  }

  "manhattanDistance" when {
    val testData = Seq(
      ((1, 2, 4, 4), 5),
      ((3, 6, 2, 1), 6),
      ((5, 5, 5, 5), 0),
      ((0, 0, 5, 2), 7)
    )

    testData.foreach(t => {
      s"locations are: A=[${t._1._1};${t._1._2}] and B=[${t._1._3};${t._1._4}]" should {
        s"return ${t._2}" in {
          val actual = Solution.manhattanDistance(t._1._1, t._1._2, t._1._3, t._1._4)
          actual shouldBe t._2
        }
      }
    })
  }

  "center" when {
    val testData = Seq(
      (7, 3),
      (21, 10),
      (3, 1),
      (1, 0)
    )

    testData.foreach(t => {
      s"n=${t._1}" should {
        s"return ${t._2}" in {
          Solution.center(t._1) shouldBe t._2
        }
      }
    })
  }
}
