package silverstar.proficiency

package object task {

  case class TestData(location: Long, expectedOutput: Long)
}
