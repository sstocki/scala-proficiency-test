## Scala Proficiency Test ##

This is a solution to proficiency test.

### Task ###

Create a console program in Scala that will answer the following question: 

Fill a two-dimensional NxN grid with NxN numbers (N is odd and the numbers are monotone increasing).

Start in the middle/center  and walk counter-clockwise to the outside. The middle square starts with 1.

    17  16  15  14  13
    18   5   4   3  12
    19   6   1   2  11
    20   7   8   9  10
    21  22  23---> ...


Now given a location (one of the cell-values), calculate the Manhattan-Distance to the center.

For example:

* From square/location 1 the distance is 0.
* From location 12 the distance is 3 (down, left, left).
* From location 23 the distance is only 2 (up twice).
* From location 1024 the distance is 31.

How many steps are required to go from location __368078__ to the center?

### Solution explanation ###

I focused on the performance of the algorithm.
I provided just basic command line app.

I made few assumptions for the algorithm:

* I do not need to create a full NxN grid to calculate the answer
* I can leverage the fact that the numbers are monotone increasing
* It is enough to travers the outer layer of the grid to calculate the result
* The grid can be traversed clockwise or anticlockwise in order to gain performance

Algorithm description:

1. first find minimum N that will fit the number
2. if N == 1 return the result
3. else calculate the bottom left and top right corners values
4. calculate if the location is closer to the bottom right or top left corner
5. choose the algorithm based on the result

### How to run? ###

* `sbt compile`
* `sbt "run <location>"`

Example: `sbt "run 368078"`

### How to run tests? ###

Tests include unit tests and performance tests.

* `sbt compile test`


