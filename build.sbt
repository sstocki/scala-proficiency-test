name := "scala-proficiency-test"

version := "0.2"

scalaVersion := "2.13.6"

mainClass in (Compile, run) := Some("silverstar.proficiency.task.SolutionApp")

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.2.10" % Test,
  "com.storm-enroute" %% "scalameter" % "0.21" % Test
)
